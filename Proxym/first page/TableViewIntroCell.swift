//
//  TableViewIntroCell.swift
//  Proxym
//
//  Created by raya naouar on 20/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class TableViewIntroCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   @IBOutlet weak var textView: UITextView!
   override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
