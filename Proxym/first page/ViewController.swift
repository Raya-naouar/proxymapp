//
//  ViewController.swift
//  Proxym
//
//  Created by raya naouar on 09/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   
   
   
   @IBOutlet weak var ViewAbout: UIView!

   
   @IBOutlet weak var Table: UITableView!
   
   
   
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   
   
  
   override func viewDidLoad() {
      super.viewDidLoad()
       ViewAbout.setGradientBackground(colorOne:color1, colorTwo: color2)
     
            Table.delegate = self
            Table.dataSource = self
    
    

   }
   
   
   
 
   
   
   
   @IBAction func Next(_ sender: UIButton) {
   
      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProxymGroup") as? ProxymGroupViewController;
     self.navigationController?.pushViewController(vc!, animated: true)
   }
   
   }





extension ViewController : UITableViewDataSource , UITableViewDelegate {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 3
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.row {
      case 0 : let cell = tableView.dequeueReusableCell(withIdentifier: "Cellinto") as! TableViewIntroCell
      cell.textView.text = """
Proxym Group is an international IT group operating in Europe, Middle East and North-West Africa with offices in Dubai, Paris and Sousse. We help our customers leverage IT innovation and mobile-enable and upgrade their organizations by providing competitive and innovative IT Know-how, solutions and services.
"""
         return cell
      case 1 : let cell = tableView.dequeueReusableCell(withIdentifier: "Cellinto") as! TableViewIntroCell
      cell.textView.text = """
We are at the crossroad of Mobile, Information System and Web. We provide services and industry solution. Our +100 skilled enthusiast engineers have successfully built over 230 projects and delivered more than 120 mobile apps for customers of various sectors and sizes.
"""
      return cell
      default : let cell = tableView.dequeueReusableCell(withIdentifier: "Cellinto") as! TableViewIntroCell
       cell.textView.text = """
“Founded in 2006 Proxym Group is an IT global player providing competitive know how, solutions and services in Europe, Arabian Gulf and North West Africa. With its different entities (Proxym-IT, Proxym France, Proxym ME and Apptiv-IT) located in France, UK, the Middle East and Tunisia, the Group specializes in enterprise solutions, mobile applications, E-commerce solutions, Medical ERPs, Social Networks and Web Portals. The Group relies on experienced, innovative and multi-lingual teams with a diversified technical expertise.”
"""

      return cell
      }
   }
   
   
}



extension UIView {
   
   func setGradientBackground(colorOne: UIColor, colorTwo: UIColor) {
      
      let gradientLayer = CAGradientLayer()
      gradientLayer.frame = bounds
      gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
      gradientLayer.locations = [0.0, 1.0]
      gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
      gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
      
      layer.insertSublayer(gradientLayer, at: 0)
   }}
extension UIColor {
   
   @objc convenience init(hexString: String,alpha: CGFloat = 1.0) {
      var cString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
      
      if (cString.hasPrefix("#")) {
         let index = cString.index(cString.startIndex, offsetBy: 1)
         cString = String(cString[index...])
      }
      
      if ((cString.count) != 6) {
         self.init(red:0, green:0, blue:0, alpha:alpha)
         return
      }
      
      var rgbValue: UInt32 = 0
      Scanner(string: cString).scanHexInt32(&rgbValue)
      
      self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,alpha: alpha)
   }
}

