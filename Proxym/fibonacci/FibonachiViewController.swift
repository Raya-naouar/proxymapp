//
//  FibonachiViewController.swift
//  Proxym
//
//  Created by raya naouar on 16/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class FibonachiViewController: UIViewController {
   
   
   let userdefaults = UserDefaults.standard
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   let encoder = JSONEncoder()
   
   
   @IBOutlet var viewFibonachi: UIView!
   @IBOutlet weak var Answer1: UITextField!
   @IBOutlet weak var Answer2: UITextField!
   @IBOutlet weak var Answer3: UITextField!
   @IBOutlet weak var Nbr2: UITextView!
   @IBOutlet weak var Nbr1: UITextView!
   @IBOutlet weak var Nbr3: UITextView!
   @IBOutlet weak var ProgressBar: UIProgressView!
   @IBOutlet weak var GoButton: UIButton!
   @IBOutlet weak var resultLabel: UILabel!
   
   
   var Score = 0
   var TabNbr :[Int] = [0,0,0]
   var person = Person(name: "", lastname: "", email: "", phone: "", brithday: "", valueRandom1: "", valueRandom2: "", valueRandom3: "", Answer1: "", Answer2: "", Answer3: "", TrueAnswer1: "", TrueAnswer2: "", TrueAnswer3: "" , date: "", Score: "")
   

   
   
   
   override func viewDidLoad() {
        super.viewDidLoad()
      
    viewFibonachi.setGradientBackground(colorOne:color2, colorTwo: color1)
    
      GoButton.layer.cornerRadius = GoButton.frame.height / 2
      

      NbrRandom()

      Nbr1.text = "n="+String(TabNbr[0])
      Nbr2.text = "n="+String(TabNbr[1])
      Nbr3.text = "n="+String(TabNbr[2])
      resultLabel.text = "0/3"
      ProgressBar.clipsToBounds = true
      ProgressBar.layer.cornerRadius = 4
      ProgressBar.transform = ProgressBar.transform.scaledBy(x: 1, y: 5)
      Answer1.delegate = self
      Answer2.delegate = self
      Answer3.delegate = self


      Answer1.addTarget(self, action: #selector(FibonachiViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
      Answer2.addTarget(self, action: #selector(FibonachiViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
      Answer3.addTarget(self, action: #selector(FibonachiViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)



    }
   
   
   
   
   func isEmpty()
   {
      if ((Answer1.text?.isEmpty)!||(Answer2.text?.isEmpty)!||(Answer3.text?.isEmpty)!){
         let alert = UIAlertController(title: "Error", message: "All fields are required", preferredStyle: .alert)
         let action = UIAlertAction(title: "Try again", style: .default, handler: nil)
         alert.addAction(action)
         present(alert, animated: true, completion: nil)
         
      }
   }
   
   
   func NbrRandom(){
    
      var nb : Int
      TabNbr[0] = Int.random(in: 1..<16)
      for i in 1...2{
          repeat {
         nb = Int.random(in: 1..<16)
         
         } while Exist(TabNbr, nb: nb, pos: i) == true
         TabNbr[i] = nb
      }
      TabNbr.sort()
      
      
   }
   func Exist (_ tab :[Int] , nb : Int, pos : Int)->Bool{
      var i = 0
      repeat {
         if tab[i] == nb {
            return true
         }
         i = i+1
      } while i != pos
      return false
   }


      func fibonacci(_ n: Int) -> Int {
         
         guard n > 1 else {return n}
         
         return fibonacci(n - 1) + fibonacci(n - 2)
         
      }
      
   
   
   func ValidationOfAnswer(Answer :Int, nbr : Int) ->Bool{
      return Answer == fibonacci(nbr)
   }
   
   
   func updatePerson()
   {
      
      let jsonString = userdefaults.string(forKey: "Users")
      let jsonData = jsonString?.data(using: .utf8)
      var users = try! JSONDecoder().decode([Person].self, from: jsonData!)
      
      for i  in 0...users.count {
         if users[i].email == person.email{
            users[i].Answer1 = Answer1.text!
            users[i].Answer2 = Answer2.text!
            users[i].Answer3 = Answer3.text!
           users[i].valueRandom1 = String(TabNbr[0])
           users[i].valueRandom2 = String(TabNbr[1])
           users[i].valueRandom3 = String(TabNbr[2])
           users[i].TrueAnswer1 = String(fibonacci(TabNbr[0]))
           users[i].TrueAnswer2 = String(fibonacci(TabNbr[1]))
           users[i].TrueAnswer3 = String(fibonacci(TabNbr[2]))
            users[i].date = Date().toString(dateFormat: "yyyy/MM/dd")
           users[i].Score = String(Score)
            
            person = users[i]
            print(person)
            break

           
         }
      }
      if let encoded = try? encoder.encode(users) {
         let jsonString = String(data: encoded, encoding: .utf8)
         userdefaults.set(jsonString, forKey: "Users")
         
      }
      let json = userdefaults.string(forKey: "Users")
      let jsonD = json?.data(using: .utf8)
      let Users = try! JSONDecoder().decode([Person].self, from: jsonD!)
      for user in Users{
         print(user)
      }
      
   }
  
   @IBAction func GoForward(_ sender: Any) {
      isEmpty()
   
      updatePerson()
     
     
      
      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Score") as? ScoreViewController;
      vc?.person = person
      self.navigationController?.pushViewController(vc!, animated: true)
      
      
   }
   func progressBar() {
      var score = 0
     ProgressBar.progress = 0
      let answer1 = Answer1.text ?? ""
       let answer2 = Answer2.text ?? ""
       let answer3 = Answer3.text ?? ""
      if  answer1 != "", ValidationOfAnswer(Answer: Int(answer1)! ,nbr: TabNbr[0] ){
         ProgressBar.progress = ProgressBar.progress + 0.333
         resultLabel.text = "1/3"
         score += 1
         print("Score1=\(score)")
         
      }
      
      if answer2 != "" , ValidationOfAnswer(Answer: Int(answer2)! , nbr: TabNbr[1]){
         
         ProgressBar.progress = ProgressBar.progress + 0.333
         resultLabel.text = "2/3"
         score += 1
         print("Score2=\(score)")
         
      }
      
      if answer3 != "" , ValidationOfAnswer(Answer: Int(answer3)!, nbr: TabNbr[2]){
         ProgressBar.progress = ProgressBar.progress + 0.333
         resultLabel.text = "3/3"
         score += 1
         print("Score3=\(score)")
         
         
      }
      Score = score
   }
  
 
   @objc func textFieldDidChange(_ textField: UITextField) {
     progressBar()
      
      
    
   }
}
extension FibonachiViewController : UITextFieldDelegate {
   func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason){
   if textField.text != "" {
      textField.isUserInteractionEnabled = false
   }
      
   }
}
