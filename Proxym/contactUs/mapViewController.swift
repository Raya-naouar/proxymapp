//
//  mapViewController.swift
//  Proxym
//
//  Created by raya naouar on 11/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit
import GoogleMaps

struct Result: Codable {
    let formattedAddress, name: String
    let location: Location

    enum CodingKeys: String, CodingKey {
        case formattedAddress = "formatted_address"
        case name, location
    }
}


struct Location: Codable {
    let lat, lng: Double
}


class mapViewController: UIViewController {

   //@IBOutlet weak var map: MKMapView!
   
   
   var mapView:GMSMapView?
   
   @IBOutlet weak var viewForMap: UIView!
   @IBOutlet weak var Footer: UIView!
   @IBOutlet weak var Header: UIView!
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
    var model : [Result]  = []
   
   
   
   override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.isNavigationBarHidden = true
      
      Header.setGradientBackground(colorOne:color1, colorTwo: color2)
      Footer.setGradientBackground(colorOne:color1, colorTwo: color2)
      
     
      mapView = GMSMapView.map(withFrame: viewForMap.frame, camera: GMSCameraPosition.camera(withLatitude: 35.820244, longitude: 10.592547, zoom:15.0))
      self.view.addSubview(mapView!)
      
      let marker = GMSMarker()
      let profileImage = UIImage(named:"location-pin")!
      let imageData = profileImage.pngData()
      marker.position = CLLocationCoordinate2D(latitude:  35.820244, longitude: 10.592547)
      marker.title = "Proxym-IT"
      marker.snippet = "Hammam Maarouf - 4000 Sousse , Tunisie"
      marker.icon = UIImage(data: imageData!, scale: 10.0)
      marker.map = mapView
    
    
    
 if let url = URL(string: "https://demo2844730.mockable.io/ProxymLocals") {
       URLSession.shared.dataTask(with: url) { data, response, error in
          if let data = data {
              do {
                self.model = try JSONDecoder().decode([Result].self, from: data)
                print(self.model)
                
            
                places()
                
                
                
              } catch let error {
                 print(error)
              }
           }
       }.resume()
    }
    
    func places() {
     
            
            DispatchQueue.main.async {
            for res in self.model
            {

              let marker = GMSMarker()
                let profileImage = UIImage(named:"location")!
                let imageData = profileImage.pngData()

                marker.position = CLLocationCoordinate2D(latitude:  res.location.lat , longitude: res.location.lng)
                marker.title = res.name
                marker.snippet = res.formattedAddress
                marker.icon = UIImage(data: imageData!, scale: 15.0)
                marker.map = self.mapView
                
            
                
                
            }
            
        }
    }
    
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
     
    }
   
   @IBAction func Previous(_ sender: Any) {
    
   _ = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactUs") as? ContactUsViewController;
      
    self.navigationController?.popViewController( animated: true)
   }

}
