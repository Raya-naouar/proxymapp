//
//  ContactUsViewController.swift
//  Proxym
//
//  Created by raya naouar on 10/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
   
   @IBOutlet var ContactUsView: UIView!
   @IBOutlet weak var Footer: UIView!
   @IBOutlet weak var Header: UIView!
   @IBOutlet weak var collectionView: UICollectionView!
   
   //COLOR BACKGROUND
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   
   //DATA OF COLLECTION VIEWS
   let collectionDataImg1 = [#imageLiteral(resourceName: "phone"),#imageLiteral(resourceName: "msg")]
   let collectionDataImg2 = [#imageLiteral(resourceName: "map"),#imageLiteral(resourceName: "internet")]
   let collectionData1 = ["Call us" ,"Mail us"]
   let collectionData2 = ["Find us","watch us"]
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.isNavigationBarHidden = true

      collectionView.dataSource = self
      collectionView.delegate = self

       Header.setGradientBackground(colorOne:color1, colorTwo: color2)
       Footer.setGradientBackground(colorOne:color1, colorTwo: color2)
      
      collectionView.register(UINib.init(nibName: "SquareCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SquareCollectionViewCell")
      collectionView.register(UINib.init(nibName: "RectangleCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RectangleCollectionViewCell")

    }

}
extension ContactUsViewController: UICollectionViewDataSource{
   func numberOfSections(in collectionView: UICollectionView) -> Int {
      return 2
   }
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return 2
   }
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      if indexPath.section == 0{
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SquareCollectionViewCell", for: indexPath) as! SquareCollectionViewCell
         let img = collectionDataImg1[indexPath.item]
         cell.Img.image = img
         let title = collectionData1[indexPath.item]
         cell.LabelImg.text = title
         
         return cell
         
      }
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RectangleCollectionViewCell", for: indexPath) as! RectangleCollectionViewCell
      let img = collectionDataImg2[indexPath.item]
      cell.Img.image = img
      let title = collectionData2[indexPath.item]
      cell.LabelImg.text = title
      
      return cell
   }
   
   
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if indexPath.section == 1 {
      if indexPath.row == 0 {
      let map = storyboard?.instantiateViewController(withIdentifier: "mapViewController") as? mapViewController
      self.navigationController?.pushViewController(map!, animated: true)
      }
      }
   }
}







extension ContactUsViewController : UICollectionViewDelegate {
   
}

extension ContactUsViewController: UICollectionViewDelegateFlowLayout{
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
      
      let width1 = Int(collectionView.frame.width/2.2)
      let width2 = Int(collectionView.frame.width)
      if indexPath.section == 0 {
         return CGSize(width:width1, height:width1-20)
         
      }
      
      
      return CGSize(width:width2-5 , height:width1-20)
      
   }
  
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      if section == 0{
         return UIEdgeInsets(top: 0, left: 4, bottom: 4, right: 4)
      }
      else
      {
         return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)}
   }
   
   @IBAction func Previous(_ sender: Any) {
  
      _ = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProxymGroup") as? ProxymGroupViewController;
      self.navigationController?.popViewController( animated: true)
   }
   
   
   @IBAction func Next(_ sender: Any) {
   
      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Register") as? RegisterViewController;
      
      self.navigationController?.pushViewController(vc!, animated: true)
   }

   
   
}


