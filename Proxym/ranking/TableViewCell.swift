//
//  TableViewCell.swift
//  Proxym
//
//  Created by raya naouar on 19/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

   
   @IBOutlet weak var Name: UILabel!
   @IBOutlet weak var Date: UILabel!
   @IBOutlet weak var Score: UILabel!
   @IBOutlet weak var Result1: UILabel!
   @IBOutlet weak var Result2: UILabel!
   @IBOutlet weak var Result3: UILabel!
   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
