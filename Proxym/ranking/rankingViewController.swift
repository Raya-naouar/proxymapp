//
//  rankingViewController.swift
//  Proxym
//
//  Created by raya naouar on 19/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class rankingViewController: UIViewController {
  
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   var Users : [Person]  = []
   var UsersValid : [Person]  = []

   
 
   @IBOutlet weak var rankingTableView: UITableView!
     @IBOutlet  weak var viewScore: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

      viewScore.setGradientBackground(colorOne:color1, colorTwo: color2)
      let json = UserDefaults.standard.string(forKey: "Users")
      let jsonD = json?.data(using: .utf8)
      Users = try! JSONDecoder().decode([Person].self, from: jsonD!)
      for user in Users{
         if (user.Answer1 != "" && user.Answer2 != "" && user.Answer3 != ""){
            UsersValid.append(user)
         }
      }
      print("HELLO1 \(Users[0])")
      
      

      rankingTableView.delegate = self
      rankingTableView.dataSource = self
      
      
     
      sortUser()
     
      print("HELLO \(UsersValid)")
 
   }
   
   func sortUser()
   {
      UsersValid.sort{
         if ($0.Score == $1.Score) && ($0.date ) == ($1.date)
         {
         
            return ($0.name.lowercased() < $1.name.lowercased())
         }
         else
         {
            if ($0.Score == $1.Score)
            {
                return ($0.date ) > ($1.date)
            }
            return ($0.Score > $1.Score)
         }
         
      }
   }
   
}
extension rankingViewController: UITableViewDelegate , UITableViewDataSource{
   public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return UsersValid.count
   }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 90
   }
   public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier:  "CellRanking") as! TableViewCell
   
      cell.Date.text = UsersValid[indexPath.row].date
      cell.Name.text = "\(UsersValid[indexPath.row].name) \(UsersValid[indexPath.row].lastname)"
      cell.Score.text = "\(UsersValid[indexPath.row].Score)/3"
      if UsersValid[indexPath.row].Score == "3" {
         cell.Score.backgroundColor =  UIColor(hexString:"#00b159")
      }
      else {
         if UsersValid[indexPath.row].Score == "0" {
            cell.Score.backgroundColor = UIColor(hexString:"#d11141")
         }
         else {
           
             cell.Score.backgroundColor =  UIColor(hexString:"#00aedb")
         }
      }
      
      
      if UsersValid[indexPath.row].Answer1 == UsersValid[indexPath.row].TrueAnswer1 {
         cell.Result1.text = " (\(UsersValid[indexPath.row].valueRandom1),\(UsersValid[indexPath.row].Answer1))"
         cell.Result1.textColor = UIColor(hexString:"#00b159")
         
      }
      else{
         cell.Result1.text = " (\(UsersValid[indexPath.row].valueRandom1),\(UsersValid[indexPath.row].Answer1))"
          cell.Result1.textColor = UIColor(hexString:"#d11141")
      }
      
      if UsersValid[indexPath.row].Answer2 == UsersValid[indexPath.row].TrueAnswer2 {
         cell.Result2.text = " (\(UsersValid[indexPath.row].valueRandom2),\(UsersValid[indexPath.row].Answer2))"
         cell.Result2.textColor = UIColor(hexString:"#00b159")
         
      }
      else{
         cell.Result2.text = " (\(UsersValid[indexPath.row].valueRandom2),\(UsersValid[indexPath.row].Answer2))"
         cell.Result2.textColor = UIColor(hexString:"#d11141")
      }
      if UsersValid[indexPath.row].Answer3 == UsersValid[indexPath.row].TrueAnswer3 {
         cell.Result3.text = " (\(UsersValid[indexPath.row].valueRandom3),\(UsersValid[indexPath.row].Answer3))"
         cell.Result3.textColor = UIColor(hexString:"#00b159")
         
      }
      else{
        
            cell.Result3.text = " (\(UsersValid[indexPath.row].valueRandom3),\(UsersValid[indexPath.row].Answer3))"
         cell.Result3.textColor = UIColor(hexString:"#d11141")
      }
         return cell
   }
   
   
   
   
   @IBAction func Previous(_ sender: Any) {
      
      _ = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Score") as? ScoreViewController;
      self.navigationController?.popViewController( animated: true)
   }
}
