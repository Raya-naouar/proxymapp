//
//  ProxymGroupViewController.swift
//  Proxym
//
//  Created by raya naouar on 10/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class ProxymGroupViewController: UIViewController {

   @IBOutlet var ProxymGoupView: UIView!
   
   
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   
   
   
   override func viewDidLoad() {
        super.viewDidLoad()
ProxymGoupView.setGradientBackground(colorOne:color1, colorTwo: color2)
        // Do any additional setup after loading the view.
      self.navigationController?.isNavigationBarHidden = true

    }
    

   
   @IBAction func Previous(_ sender: Any) {
     
      _ = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstPage") as? ViewController;
      self.navigationController?.popViewController( animated: true)
   }
   

   @IBAction func Next(_ sender: Any) {
      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactUs") as? ContactUsViewController;
      
      self.navigationController?.pushViewController(vc!, animated: true)
   }
}
