//
//  ScoreViewController.swift
//  Proxym
//
//  Created by raya naouar on 18/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {
   
   
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   
   @IBOutlet var viewScore: UIView!
   @IBOutlet weak var NameLabel: UILabel!
   @IBOutlet weak var ScoreTableView: UITableView!
   
   
   var person = Person(name: "", lastname: "", email: "", phone: "", brithday: "", valueRandom1: "", valueRandom2: "", valueRandom3: "", Answer1: "", Answer2: "", Answer3: "", TrueAnswer1: "", TrueAnswer2: "", TrueAnswer3: "" , date: "", Score: "")

   
   
   
   
   override func viewDidLoad() {
      super.viewDidLoad()

      ScoreTableView.delegate = self
      ScoreTableView.dataSource = self
      NameLabel.text = "Bonjour " + person.name
      viewScore.setGradientBackground(colorOne:color1, colorTwo: color2)

      
   }

   
   
   @IBAction func next(_ sender: Any) {
      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RankingViewController") as? rankingViewController;
      
      self.navigationController?.pushViewController(vc!, animated: true)
   }
   
}

extension ScoreViewController : UITableViewDelegate , UITableViewDataSource{
   public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 5
   }
   
   public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      switch indexPath.row {
         
      case 0 :
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellTwoID") as! ModelTwoTableViewCell
         cell.Label1.text = "Date :"
         cell.Label2.text = Date().toString(dateFormat: "MMM dd,yyyy")
         cell.Label3.isHidden = true
         cell.backgroundColor = UIColor(hexString:"#4B2F54")
         return cell
         
      case 1 :
         let cell = tableView.dequeueReusableCell(withIdentifier:  "cellTwoID") as! ModelTwoTableViewCell
         cell.Label1.text = "n = \(person.valueRandom1):"
         cell.Label2.text = person.Answer1
         if  person.Answer1 != person.TrueAnswer1 {
            cell.Label3.text = person.TrueAnswer1
             cell.backgroundColor = UIColor(hexString:"#650816")
         }
         else{
            cell.Label3.isHidden = true
            cell.backgroundColor = UIColor(hexString:"#035909")
         }
         return cell
      case 2 :let cell = tableView.dequeueReusableCell(withIdentifier: "cellTwoID") as! ModelTwoTableViewCell
      cell.Label1.text = "n = \(person.valueRandom2):"
      cell.Label2.text = person.Answer2
      if  person.Answer2 != person.TrueAnswer2 {
         cell.Label3.text = person.TrueAnswer2
             cell.backgroundColor = UIColor(hexString:"#650816")
      }else{
           cell.Label3.isHidden = true
      cell.backgroundColor = UIColor(hexString:"#035909")
      }
      return cell
      case 3 :
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellTwoID") as! ModelTwoTableViewCell
         cell.Label1.text = "n = \(person.valueRandom3):"
         cell.Label2.text = person.Answer3
         if  person.Answer3 != person.TrueAnswer3 {
            cell.Label3.text = person.TrueAnswer3
                cell.backgroundColor = UIColor(hexString:"#650816")
         }
         else{
            cell.Label3.isHidden = true
         cell.backgroundColor = UIColor(hexString:"#035909")
         }
         return cell
         
         
      default :
         
         return tableView.dequeueReusableCell(withIdentifier: "ModelOne") as! ModelOneTableViewCell
         
      }
   }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 50
   }
   
}
extension Date
{
   func toString( dateFormat format  : String ) -> String
   {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = format
      return dateFormatter.string(from: self)
   }
}
