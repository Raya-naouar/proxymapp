//
//  RegisterViewController.swift
//  Proxym
//
//  Created by raya naouar on 10/12/2019.
//  Copyright © 2019 raya naouar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


struct Person: Codable {
   var name: String
   var lastname:  String
   var email:  String
   var phone: String
   var brithday:  String
   var valueRandom1: String
   var valueRandom2: String
   var valueRandom3: String
   var Answer1 : String
   var Answer2: String
   var Answer3 : String
   var TrueAnswer1 : String
   var TrueAnswer2 :String
   var TrueAnswer3 : String
   var date : String
   var Score : String
   
   
}

class RegisterViewController: UIViewController {
   

   @IBOutlet weak var PhoneTextField: UITextField!
   @IBOutlet weak var BirthdayTextField: UITextField!
   @IBOutlet weak var EmailTextField: UITextField!
   @IBOutlet weak var LastNameTextField: UITextField!
   @IBOutlet weak var NameTextField: UITextField!
   @IBOutlet weak var ClearButton: UIButton!
   @IBOutlet weak var OkButton: UIButton!
   @IBOutlet var RegisterViewController: UIView!
   
   
   let color1 = UIColor(hexString:"#2BC0E4")
   let color2 = UIColor(hexString:"#42254C")
   let color3 = UIColor(hexString:"#9B9CA0")
   let userdefaults = UserDefaults.standard
   let encoder = JSONEncoder()
   
   
   
   var arrayPerson : [Person]  = []
   var person = Person(name: "", lastname: "", email: "", phone: "", brithday: "", valueRandom1: "", valueRandom2: "", valueRandom3: "", Answer1: "", Answer2: "", Answer3: "", TrueAnswer1: "", TrueAnswer2: "", TrueAnswer3: "" , date: "", Score: "")
   
 
   lazy var datePicker: UIDatePicker = {
      let picker = UIDatePicker()
      picker.datePickerMode = .date
      picker.addTarget(self, action: #selector(datePickerChanged(_:)), for: .valueChanged)
      return picker
   }()
   lazy var dateFormatter: DateFormatter = {
      
      let formatter = DateFormatter()
      
      formatter.dateStyle = .medium
      
      formatter.timeStyle = .none
      
      return formatter
   }()
   

   
   override func viewDidLoad() {
      super.viewDidLoad()
      IQKeyboardManager.shared.enable = true
      
      self.datePicker.maximumDate = Date()
      BirthdayTextField.inputView = datePicker
      
      self.navigationController?.isNavigationBarHidden = true
   RegisterViewController.setGradientBackground(colorOne:color1, colorTwo: color2)
      StyleButton()
      StyleTextField()
      
      
      
     if let jsonString = userdefaults.string(forKey: "Users")
         {
            let jsonData = jsonString.data(using: .utf8)
         arrayPerson = try! JSONDecoder().decode([Person].self, from: jsonData!)
            print("1",arrayPerson)
    }
    
     
      
   }
   
   @objc func datePickerChanged(_ sender: UIDatePicker) {
      BirthdayTextField.text = formateDate(sender.date)
   }
   
   
     func formateDate(_ date: Date) -> String {
      let dateFormatter = DateFormatter()
      dateFormatter.locale = Locale.current
      dateFormatter.dateFormat = "yyyy-MM-dd"
      return dateFormatter.string(from: date)
      
   }
   
  

   
   
   //Style
   
   func StyleButton()  {
      OkButton.layer.cornerRadius = OkButton.frame.height / 2
      ClearButton.layer.cornerRadius = ClearButton.frame.height / 2
      
   }
   func StyleTextField ()
   {
      NameTextField.layer.borderWidth = 1
      NameTextField.layer.borderColor  = color3.cgColor
      LastNameTextField.layer.borderWidth = 1
      LastNameTextField.layer.borderColor  = color3.cgColor
      EmailTextField.layer.borderWidth = 1
      EmailTextField.layer.borderColor  = color3.cgColor
      PhoneTextField.layer.borderWidth = 1
      PhoneTextField.layer.borderColor  = color3.cgColor
      BirthdayTextField.layer.borderWidth = 1
      BirthdayTextField.layer.borderColor  = color3.cgColor
   }
   
   
   
   
   
   //Validation
   
   func existEmail(email : String)->Bool{
      for user in arrayPerson
      {
         if (user.email == email)
         { return true }
         
      }
      
      return false
      
    
   }
   func isValidEmail(testStr:String) -> Bool {
     
      print("validate emilId: \(testStr)")
      let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
      let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
      let result = emailTest.evaluate(with: testStr)
      return result
   }
   
   
   func validatePhoneNbr(value: String) -> Bool {
      let PHONE_REGEX = "[0-9]{8,8}$"
      let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
      let result =  phoneTest.evaluate(with: value)
      return result
   }
   
   func validateName(value: String) -> Bool {
      let PHONE_REGEX = "[A-Za-z]{3,15}$"
      let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
      let result =  phoneTest.evaluate(with: value)
      return result
   }
   

   
   func isEmpty()
   {
      if ((NameTextField.text?.isEmpty)!||(LastNameTextField.text?.isEmpty)!||(EmailTextField.text?.isEmpty)!||(PhoneTextField.text?.isEmpty)!||(BirthdayTextField.text?.isEmpty)!){
         let alert = UIAlertController(title: "Error", message: "All fields are required", preferredStyle: .alert)
         let action = UIAlertAction(title: "Try again", style: .default, handler: nil)
         alert.addAction(action)
         present(alert, animated: true, completion: nil)
         
      }
   }
   
   

   func setPerson()
   {
      person.name = NameTextField.text!
      person.brithday = BirthdayTextField.text!
      person.lastname = LastNameTextField.text!
      person.email = EmailTextField.text!
      person.phone = PhoneTextField.text!
      
   
      
      arrayPerson.append(person)
      
      
      
      if let encoded = try? encoder.encode(arrayPerson) {
         let jsonString = String(data: encoded, encoding: .utf8)
         userdefaults.set(jsonString, forKey: "Users")
      }
      print("2",arrayPerson)
   }
   
   
   
   @IBAction func Ok(_ sender: Any) {
      isEmpty()
   
      if (validateName(value: NameTextField.text!) == false){
         let alert = UIAlertController(title: "Error", message: "Name not valide", preferredStyle: .alert)
         let action = UIAlertAction(title: "Try again", style: .default, handler: {
            action in
            self.NameTextField.text=""
         })
         alert.addAction(action)
         present(alert, animated: true, completion: nil)
         
      }
      else {
            if (validateName(value: LastNameTextField.text!) == false){
               let alert = UIAlertController(title: "Error", message: "Lastname not valide", preferredStyle: .alert)
               let action = UIAlertAction(title: "Try again", style: .default, handler: {
                  action in
                  self.LastNameTextField.text=""
               })
               alert.addAction(action)
               present(alert, animated: true, completion: nil)
               
               }
            else {
      
                     if (isValidEmail(testStr: EmailTextField.text!) == false){
                         let alert = UIAlertController(title: "Error", message: "E-mail not valide", preferredStyle: .alert)
                        let action = UIAlertAction(title: "Try again", style: .default, handler: {
                           action in
                           self.EmailTextField.text=""
                        })
                        alert.addAction(action)
                         present(alert, animated: true, completion: nil)
                     }  else
                     {
                        if (existEmail(email: EmailTextField.text!) == true){
                           let alert = UIAlertController(title: "Error", message: "E-mail exist", preferredStyle: .alert)
                           let action = UIAlertAction(title: "Try again", style: .default, handler: {
                              action in
                              self.EmailTextField.text=""
                           })
                           alert.addAction(action)
                           present(alert, animated: true, completion: nil)
                        }
                        
                     else {
                     
                        if (validatePhoneNbr(value: PhoneTextField.text!) == false){
                           let alert = UIAlertController(title: "Error", message: "Phone number not valide", preferredStyle: .alert)
                           let action = UIAlertAction(title: "Try again", style: .default, handler: {
                              action in
                              self.PhoneTextField.text=""
                           })
                           alert.addAction(action)
                            present(alert, animated: true, completion: nil)
                           
                        }
                        else {
                           setPerson()
                           let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Fibonachi") as? FibonachiViewController;
                           vc?.person = person
                           self.navigationController?.pushViewController(vc!, animated: true)
                           let jsonString = userdefaults.string(forKey: "Users")
                           let jsonData = jsonString?.data(using: .utf8)
                           let users = try! JSONDecoder().decode([Person].self, from: jsonData!)
                           for user in users {
                              print(user)
                           }
                        }
                        }}}}
   
      
      
   }
   
   
   
   
   
   
         @IBAction func Clear(_ sender: Any) {
            NameTextField.text=""
            LastNameTextField.text=""
            EmailTextField.text=""
            PhoneTextField.text=""
            BirthdayTextField.text=""
         
         }
   
   
   
         @IBAction func Previous(_ sender: Any) {
            _ = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactUs") as? ContactUsViewController;
            self.navigationController?.popViewController( animated: true)
         }

   
}
